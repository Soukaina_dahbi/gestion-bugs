package com.lamrani.config.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class CorsConfig {

    @Value("${cors.allowed.origins}")
    private String allowedOrigins;

    @Value("#{'${cors.allowed.methods}'.split(',')}")
    private List<String> allowedMethods;

    @Bean
    public WebMvcConfigurer mvcConfigurer() {
        //TODO Replace it
        return new WebMvcConfigurerAdapter() {
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods(allowedMethods.toArray(new String[allowedMethods.size()]))
                        .allowedOrigins(allowedOrigins);
            }
        };
    }
}
