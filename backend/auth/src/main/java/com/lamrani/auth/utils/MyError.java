package com.lamrani.auth.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;

/**
 * <h1>Error generator</h1>
 *
 * @author  Mohamed Lamrani Alaoui
 * @author Zemouri Badr
 * @version 1.0
 * @since   2018-05-16
 */
public class MyError {
	int error;
	private String message;
	public int getError() {
		return error;
	}
	public void setError(int error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MyError(int error, String message) {
		this.error = error;
		this.message = message;
	}
	
	public static ModelMap getErrorModel(int error, String message){
		MyError err= new MyError(error, message);
		ModelMap model = new ModelMap();
	    model.put("error",err);
	    return model;
	}
	public static ResponseEntity<?> getErrorResponse(HttpStatus status, String message){
		MyError err= new MyError(status.value(), message);
		ModelMap model = new ModelMap();
	    model.put("error",err);
	    return new ResponseEntity<>(model, new HttpHeaders(), status);
	}
	
	public static ResponseEntity<?> getNotFound(String message){
	    return getErrorResponse(HttpStatus.NOT_FOUND, message);
	}
	
	public static ResponseEntity<?> getUnauthorized(String message){
	    return getErrorResponse(HttpStatus.UNAUTHORIZED, message);
	}
	
	public static ResponseEntity<?> getConflict(String message){
	    return getErrorResponse(HttpStatus.CONFLICT, message);
	}
	
	public static ResponseEntity<?> getBadRequest(String message){
	    return getErrorResponse(HttpStatus.BAD_REQUEST, message);
	}
	
	public static ResponseEntity<?> getForbidden(String message){
	    return getErrorResponse(HttpStatus.FORBIDDEN, message);
	}
}
