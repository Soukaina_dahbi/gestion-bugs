package com.lamrani.auth.service.interfaces;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.multipart.MultipartFile;

import com.lamrani.auth.config.JwtUser;
import com.lamrani.auth.model.User;

/**
* <h1>User management interface</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
public interface UserServiceInterface {

	/**
	 * Get user by email
	 * @param email
	 * @return User
	 * @throws UsernameNotFoundException
	 */
	User loadUserByEmail(String email) throws UsernameNotFoundException;

	/**
	 * Get JWT user instance
	 * @param request
	 * @return JwtUser
	 */
	JwtUser getAuthenticatedJwtUser(HttpServletRequest request);
	
	/**
	 * Get current user
	 * @return User
	 */
	User getAuthenticatedUser();
	
	/**
	 * Change password
	 * @param user the user to update
	 * @param password the password should have at least 6 character and at most 100 character
	 * @return ResponseEntity with following status: <br/>
	 * 400: Bad or missing data <br/>
	 * 200: User
	 */
	ResponseEntity<?> changePassword(User user, String password);
	
	/**
	 * Change user info
	 * The password can be null if it's not meant to be edited
	 * @param user the user to update
	 * @param newValues the new user values
	 * @return ResponseEntity with following status: <br/>
	 * 409: Email already exist <br/>
	 * 200: User
	 */
	ResponseEntity<?> changeInfo(User user, User newValues);
	
	/**
	 * Change user avatar
	 * @param user the user to update
	 * @param file the new image to be set
	 * @return ResponseEntity with following status: <br/>
	 * 200: User
	 */
	ResponseEntity<?> changeAvatar(User user, MultipartFile file);
	
	/**
	 * Delete user avatar
	 * @param user the user to update
	 * @return ResponseEntity with following status: <br/>
	 * 200: User
	 */
	ResponseEntity<?> deleteAvatar(User user);
	
	/**
	 * Send email containing token to following email
	 * @param email
	 * @return ResponseEntity with following status: <br/>
	 * 404: User not found <br/>
	 * 204: Success
	 */
	ResponseEntity<?> forgetPassword(String email);
	
	/**
	 * Reset password using the generated token sended in mail
	 * @param token
	 * @param password
	 * @return ResponseEntity with following status: <br/>
	 * 400: Bad or missing data <br/>
	 * 404: User not found <br/>
	 * 204: Success
	 */
	ResponseEntity<?> resetPassword(String token, String password);
}
