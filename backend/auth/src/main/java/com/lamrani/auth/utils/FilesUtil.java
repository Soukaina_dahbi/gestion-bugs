package com.lamrani.auth.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * <h1>Files helper</h1>
 *
 * @author  Mohamed Lamrani Alaoui
 * @author Zemouri Badr
 * @version 1.0
 * @since   2018-05-16
 */
public class FilesUtil {

	private static final String urlProfil = File.separator+"peaqock_import"+File.separator+"profil"+File.separator;

	/**
	 * Move the file to profile folder using the specified name 
	 * @param file
	 * @param name
	 * @return boolean If the file is been moved correctly
	 * @throws IOException
	 */
	public static boolean moveFile(MultipartFile file, String name) throws IOException {
		String url = getProfilUrl()+name;
		File fileToSave = new File(url);
		fileToSave.createNewFile();
		FileOutputStream fos = new FileOutputStream(fileToSave); 
		fos.write(file.getBytes());
		fos.close();
		return true;
	}

	/**
	 * Get the file extension from the file name
	 * @param file
	 * @return
	 */
	public static String getFileExtension(MultipartFile file) {
		String orgName = file.getOriginalFilename();
		String[] parts = orgName.split("\\.");
		String extension = parts[parts.length-1];
		return extension;
	}

	/**
	 * Delete the file from the profile folder  using the specified name 
	 * @param name
	 * @return boolean If the file is been removed correctly
	 */
	public static boolean deleteFile(String name){
		String url = getProfilUrl()+name;
		File fileToSave = new File(url);
		if(fileToSave.exists()) {
			return fileToSave.delete();
		}
		return false;
	}

	/**
	 * Get the profile url folder
	 * @return String url
	 */
	public static String getProfilUrl() {
		return new File("").getAbsolutePath()+urlProfil;
	}
}
