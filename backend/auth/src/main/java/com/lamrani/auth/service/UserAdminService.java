package com.lamrani.auth.service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.lamrani.auth.email.EmailHtmlSender;
import com.lamrani.auth.mapping.ErrorName;
import com.lamrani.auth.model.Authority;
import com.lamrani.auth.model.AuthorityName;
import com.lamrani.auth.model.User;
import com.lamrani.auth.repositories.AuthorityRepository;
import com.lamrani.auth.repositories.UserRepository;
import com.lamrani.auth.service.interfaces.UserAdminServiceInterface;
import com.lamrani.auth.utils.MyError;
import com.lamrani.auth.utils.ResponseUtil;
import com.lamrani.auth.utils.SecurityUtil;

/**
* <h1>User admin management</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Service
public class UserAdminService implements UserAdminServiceInterface {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AuthorityRepository authorityRepository;

	@Autowired
	private EmailHtmlSender emailHtmlSender;

	@Autowired
	SecurityUtil securityService;
	
	@Value("${app.email-object}")
    private String appObjectName;

	@Override
	public List<User> findAll() {
		Collection<AuthorityName> col= new LinkedList<AuthorityName>();
		col.add(AuthorityName.ROLE_SUPER_ADMIN);
		List<User> users= userRepository.findNotAdmin(col);
		for (User user : users) {
			user.setImage(null);
		}
		return users;
	}

	@Override
	public ResponseEntity<?> addUser(User input) {
		User user= userRepository.findByEmail(input.getEmail());
		if(user!=null) {
			return MyError.getConflict(ErrorName.USER_ALREADY_EXIST.toString());
		}
		if(input.getPassword()==null) {
			return MyError.getBadRequest(ErrorName.USER_PASSWORD_REQUIRED.toString());
		}

		BCryptPasswordEncoder bc= new BCryptPasswordEncoder();
		String t= bc.encode(input.getPassword());

		User newUser= new User();
		newUser= userInitHelper(input, newUser, t);
		user= userRepository.save(newUser);

		sendMail(user, input.getPassword());
		return ResponseUtil.ok(user);
	}

	@Override
	public ResponseEntity<?> editUser(User input) {
		Optional<User> oldUser= userRepository.findById(input.getId());
		if(!oldUser.isPresent()) {
			return MyError.getNotFound(ErrorName.USER_NOT_FOUND.toString());
		}
		if(securityService.userHasRole(oldUser.get(), AuthorityName.ROLE_SUPER_ADMIN)) {
			return MyError.getForbidden(ErrorName.USER_FORBIDDEN_SUPER_ADMIN.toString());
		}else {
			String oldEmail= oldUser.get().getEmail();
			if(!oldEmail.equalsIgnoreCase(input.getEmail())) {
				User oldUserEmail= userRepository.findByEmail(input.getEmail());
				if(oldUserEmail!=null) {
					return MyError.getConflict(ErrorName.USER_ALREADY_EXIST.toString());
				}
			}
			User newUser= oldUser.get();
			BCryptPasswordEncoder bc= new BCryptPasswordEncoder();
			String t;
			String pass= "";
			if(input.getPassword()!=null) {
				t= bc.encode(input.getPassword());
				pass= input.getPassword();
			}else {
				t=newUser.getPassword();
			}
			newUser= userInitHelper(input, newUser, t);
			newUser= userRepository.save(newUser);
			if(!input.getEmail().equals(oldEmail) || input.getPassword()!=null) {
				sendMail(newUser,pass);
			}
			newUser.setImage(null);
			return ResponseUtil.ok(newUser);
		}
	}

	@Override
	public ResponseEntity<?> delete(Long id) {
		Optional<User> user= userRepository.findById(id);
		if(user.isPresent()) {
			User foundUser= user.get();
			if(securityService.userHasRole(foundUser, AuthorityName.ROLE_SUPER_ADMIN)) {
				return MyError.getForbidden(ErrorName.USER_FORBIDDEN_SUPER_ADMIN.toString());
			}else {
				userRepository.deleteById(id);
				return ResponseUtil.noContent();
			}
		}else {
			return MyError.getNotFound(ErrorName.USER_NOT_FOUND.toString());
		}
	}

	@Override
	public ResponseEntity<?> enable(Long id, boolean enable) {
		Optional<User> user= userRepository.findById(id);
		if(user.isPresent()) {
			User userEnable= user.get();
			if(securityService.userHasRole(userEnable, AuthorityName.ROLE_SUPER_ADMIN)) {
				return MyError.getForbidden(ErrorName.USER_FORBIDDEN_SUPER_ADMIN.toString());
			}else {
				userEnable.setEnabled(enable);
				userRepository.save(userEnable);
				return ResponseUtil.noContent();
			}
		}else {
			return MyError.getNotFound(ErrorName.USER_NOT_FOUND.toString());
		}
	}

	@Override
	public ResponseEntity<?> findOne(Long id) {
		Optional<User> user= userRepository.findById(id);
		if(user.isPresent()) {
			User foundUser= user.get();
			if(securityService.userHasRole(foundUser, AuthorityName.ROLE_SUPER_ADMIN)) {
				return MyError.getForbidden(ErrorName.USER_FORBIDDEN_SUPER_ADMIN.toString());
			}else {
				foundUser.setImage(null);
				return ResponseUtil.ok(foundUser);
			}
		}else {
			return MyError.getNotFound(ErrorName.USER_NOT_FOUND.toString());
		}
	}

	/**
	 * Get authorities object for the new user
	 * @param input
	 * @return List Authority
	 */
	private List<Authority> searchForAuth(User input) {
		if(input.getAuthorities()!=null) {
			Collection<AuthorityName> per= new LinkedList<AuthorityName>();
			AuthorityName admin= AuthorityName.ROLE_SUPER_ADMIN;
			for (Authority authorityItem : input.getAuthorities()) {
				if(authorityItem.getName()!=admin) {
					per.add(authorityItem.getName());
				}
			};
			return authorityRepository.findByNameIn(per);
		}
		return null;
	}

	/**
	 * Map the user data and password into the new user object 
	 * @param input
	 * @param newUser
	 * @param password
	 * @return
	 */
	private User userInitHelper(User input, User newUser, String password) {
		newUser.setEmail(input.getEmail());
		newUser.setPassword(password);
		newUser.setFirstname(input.getFirstname());
		newUser.setLastname(input.getLastname());
		newUser.setEnabled(input.getEnabled());
		newUser.setAuthorities(searchForAuth(input));
		return newUser;
	}

	
	/**
	 * Send mail containing the user info: email and password
	 * @param user
	 * @param password
	 */
	private void sendMail(User user,String password) {
		final Context ctx = new Context();
		ctx.setVariable("username", user.getEmail());
		ctx.setVariable("password", password);
		ctx.setVariable("name", user.getFirstname()+" "+user.getLastname());
		emailHtmlSender.send(user.getEmail(), appObjectName, "email/email-template", ctx);
	}

}
