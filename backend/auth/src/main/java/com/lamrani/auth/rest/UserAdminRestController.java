package com.lamrani.auth.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lamrani.auth.model.User;
import com.lamrani.auth.service.interfaces.UserAdminServiceInterface;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/admin/user")
@CrossOrigin("*")
public class UserAdminRestController {
    
    @Autowired
    private UserAdminServiceInterface userAdminService;
	
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all users, return users")
	public ResponseEntity<?> getAllUsers() {
		return new ResponseEntity<>(userAdminService.findAll(), new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "Get user by id, return user or status: 403, 404")
	public ResponseEntity<?> getOneUser(@PathVariable("id") Long id) {
		return userAdminService.findOne(id);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Add new user, return user or status: 400, 403, 409")
	public ResponseEntity<?> addUser(@Valid @RequestBody User input, Errors errors) {
		if (errors.hasErrors()) {
			return new ResponseEntity<>(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
		}
		return userAdminService.addUser(input);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	@ApiOperation(value = "Edit user by id, return user or status: 400, 403, 404, 409")
	public ResponseEntity<?> editUser(@Valid @RequestBody User input, Errors errors) {
		if (errors.hasErrors()) {
			return new ResponseEntity<>(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
		}
		return userAdminService.editUser(input);
	}
	
	@RequestMapping(value="/{id}" ,method = RequestMethod.DELETE)
	@ApiOperation(value = "delete user by id, return 204 or status: 403, 404")
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
		return userAdminService.delete(id);
	}

	@RequestMapping(value="/{id}/enable" ,method = RequestMethod.GET)
	@ApiOperation(value = "enable user by id, return 204 or status: 403, 404")
	public ResponseEntity<?> enableUser(@PathVariable("id") Long id) {
		return userAdminService.enable(id, true);
	}
	
	@RequestMapping(value="/{id}/disable" ,method = RequestMethod.GET)
	@ApiOperation(value = "disable user by id, return 200 or status: 403, 404")
	public ResponseEntity<?> disableUser(@PathVariable("id") Long id) {
		return userAdminService.enable(id, false);
	}
	
}
