package com.lamrani.auth.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN, ROLE_SUPER_ADMIN
}