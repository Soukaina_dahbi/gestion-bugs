package com.lamrani.importexcels.rest;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.lamrani.auth.utils.MyError;

import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/export")
public class ExportExcelsCanvasController {

    @GetMapping(value = "/{excelName}")
    public ResponseEntity<?> getImageWithMediaType(@PathVariable("excelName") String excelName) throws IOException {

        final InputStream in = getClass().getResourceAsStream("/static/"+excelName+".xlsx");
        InputStreamResource resource = new InputStreamResource(in);
        
		ModelMap model = new ModelMap();
	    model.put("body",org.apache.commons.io.IOUtils.toByteArray(in));

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(model, headers, HttpStatus.OK);
    }



}
