package com.lamrani.domaine.services;

import com.lamrani.domaine.model.Description;
import com.lamrani.domaine.repositories.DescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class DescriptionService {
    @Autowired

    DescriptionRepository descriptionRepository;

    public ResponseEntity<?> add(Description description ){

        if(description.getIdD()!=null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(descriptionRepository.save(description),HttpStatus.OK);
    }

    public List<Description> getAll(){

        return  descriptionRepository.findAll();
    }


    public ResponseEntity<?> find(Long idD) {
        Optional<Description> description = descriptionRepository.findById(idD);
        if (description.isPresent()) {
            return new ResponseEntity<>(description.get(), HttpStatus.OK);
        }
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("error", "object non trouve");
        return new ResponseEntity<>(stringStringHashMap,HttpStatus.NOT_FOUND);
    }
    public Description edit(Description description){
        if(description.getIdD() == null || find(description.getIdD())==null)
            return description;
        return descriptionRepository.save(description);
    }

    public void delete(Long id){
        Optional<Description> description =  descriptionRepository.findById(id);
        if(description.isPresent())
            descriptionRepository.delete(description.get());
    }
}
