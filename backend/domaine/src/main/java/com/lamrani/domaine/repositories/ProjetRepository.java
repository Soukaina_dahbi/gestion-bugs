package com.lamrani.domaine.repositories;

import com.lamrani.domaine.model.Projet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
public interface ProjetRepository extends JpaRepository<Projet,Long> {

   Optional<Projet> findById(Long Id);
    List<Projet> findBynom(String nom);
}
