package com.lamrani.domaine.repositories;


import org.springframework.context.annotation.Description;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DescriptionRepository extends JpaRepository<Description,Long> {
  Optional<Description>  findByIdD(Long Idb);
    List<Description>findByMessage(String message);
}
