package com.lamrani.domaine.services;

import com.lamrani.domaine.model.Bug;
import com.lamrani.domaine.model.Projet;
import com.lamrani.domaine.repositories.BugRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class BugService {

	@Autowired
	BugRepository bugRepository;

	@Autowired
	ProjetService projectService;

	public List<Bug> getAll() {

		return bugRepository.findAll();
	}

	public ResponseEntity<?> add(Bug bug, Long idProjet) {
		Projet projet = projectService.find(idProjet);
		bug.setProjet(projet);
		if (bug.getIdB() != null) {

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);


		}

		return new ResponseEntity<>(bugRepository.save(bug), HttpStatus.OK);
	}

	public ResponseEntity<?> find(Long id) {
		Optional<Bug> bug = bugRepository.findById(id);
		if (bug.isPresent()) {
			return new ResponseEntity<>(bug.get(), HttpStatus.OK);
		}
		HashMap<String, String> stringStringHashMap = new HashMap<>();
		stringStringHashMap.put("error", "object non trouve");
		return new ResponseEntity<>(stringStringHashMap, HttpStatus.NOT_FOUND);
	}

	public Bug edit(Bug bug) {
		if (bug.getIdB() == null || find(bug.getIdB()) == null)
			return bug;
		return bugRepository.save(bug);
	}

	public void delete(Long id) {
		Optional<Bug> bug = bugRepository.findById(id);
		if (bug.isPresent())
			bugRepository.delete(bug.get());
	}

	public List<Bug> searchType(String type) {
		return bugRepository.findAllByType(type);
	}

	public List<Bug> searchDate(Date date) {
		return bugRepository.findByDate(date);
	}

	public List<Bug> searchStatut(String statut) {
		return bugRepository.findByStatut(statut);
	}

}
