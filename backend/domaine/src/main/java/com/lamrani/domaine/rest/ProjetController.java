package com.lamrani.domaine.rest;

import com.lamrani.domaine.model.Projet;
import com.lamrani.domaine.services.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("projet")
public class ProjetController {
    @Autowired
    private ProjetService projetService;

    @PostMapping("")
    public Projet add(@RequestBody Projet projet)
    {
        return projetService.add(projet);
    }

    @GetMapping("/find/{id}")
    public Projet find(@PathVariable Long id){return  projetService.find(id);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { projetService.delete(id);}

    @PutMapping("")
    public Projet edite(@RequestBody Projet projet){ return projetService.edit(projet);}


    @GetMapping("/search")
    public List<Projet> find(@RequestParam String nom){
        return projetService.search(nom);
    }

    @GetMapping("")
    public List<Projet> getAll() {
        return projetService.getAll();}

}
