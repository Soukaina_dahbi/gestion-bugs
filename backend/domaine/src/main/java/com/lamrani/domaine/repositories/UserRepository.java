package com.lamrani.domaine.repositories;


import com.lamrani.domaine.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {

    List<User> findBynom(String nom);

}
