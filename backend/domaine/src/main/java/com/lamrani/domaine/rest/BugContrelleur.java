package com.lamrani.domaine.rest;

import com.lamrani.domaine.model.Bug;
import com.lamrani.domaine.repositories.BugRepository;
import com.lamrani.domaine.repositories.ProjetRepository;
import com.lamrani.domaine.services.BugService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("bug")
public class BugContrelleur {
    @Autowired
    BugService bugService;


    @Autowired
    private BugRepository bugRepository;

    @Autowired
    private ProjetRepository projetRepository;




    @GetMapping("")
    public List<Bug> getAll() {
        return bugService.getAll();}

    @PostMapping("{idProjet}")
    public ResponseEntity<?> add(@RequestBody Bug bug, @PathVariable Long idProjet)
    {

        return bugService.add(bug,idProjet);
    }
    @PutMapping("")
    public  Bug edite(@RequestBody Bug bug){ return bugService.edit(bug);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { bugService.delete(id);}
    @GetMapping("/searchType")
    public List<Bug> searchType(@RequestParam String type){return bugService.searchType(type);}
    @GetMapping("/searchStatut")
    public List<Bug> searchStatut(@RequestParam String statut){return bugService.searchStatut(statut);}
    @GetMapping("/searchDate")
    public List<Bug> searchDate(@RequestParam Date date){return bugService.searchDate(date);}
}
