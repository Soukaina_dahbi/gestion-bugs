package com.lamrani.domaine.repositories;

import com.lamrani.domaine.model.Bug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")


@RepositoryRestResource(collectionResourceRel = "bugs", path = "bugs")
public interface BugRepository extends JpaRepository<Bug, Long> {

	List<Bug> findAllByType(String type);

	List<Bug> findByDate(Date date);

	List<Bug> findByStatut(String statut);

	Bug findByIdB(Long id);

}
