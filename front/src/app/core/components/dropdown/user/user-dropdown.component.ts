import { Component, OnInit, HostListener, Input } from '@angular/core';
import { User } from '../../../models/user';
import { UserStoreService } from '../../../store/user-store.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.css']
})
export class UserDropdownComponent implements OnInit {
  @Input('user') user: User;
  hidden:boolean= true;
  constructor(private userService: UserService) { }

  ngOnInit() {}

  showMenu($event: Event){
    this.hidden=!this.hidden;
    $event.preventDefault();
    $event.stopPropagation();
  }
  @HostListener('document:click', ['$event']) clickedOutside($event){
    this.hidden= true;
  }

  logout(){
    this.userService.logout();
  }
}
