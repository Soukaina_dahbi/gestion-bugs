import { Component, OnInit, HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { LicenceService } from '../../../service/licence.service';
import * as moment from 'moment';

@Component({
  selector: 'notification-dropdown',
  templateUrl: './notification-dropdown.component.html',
  styleUrls: ['./notification-dropdown.component.css']
})
export class NotificationDropdownComponent implements OnInit {
  hidden: boolean = true;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );
list:any[];
dely:number=3;

constructor(private breakpointObserver: BreakpointObserver,private licenceservice:LicenceService) {
  this.licenceservice.getData.subscribe(data=>{ 
    this.list =data;  
  })
}
  ngOnInit() {
    
  }
  showMenu($event: Event) {
    this.hidden = !this.hidden;
    $event.preventDefault();
    $event.stopPropagation();
  }
  @HostListener('document:click', ['$event']) clickedOutside($event) {
    this.hidden = true;
  }
 
  getDate(date:any){  
    return ( moment(date).diff(moment(),'days')>=0  && moment(date).diff(moment(),'days')<=this.dely );
    }
  
  getCount(){
    return this.list.filter(e=>( moment(e.dateEx).diff(moment(),'days')>=0  && moment(e.dateEx).diff(moment(),'days')<=this.dely )).length;
    }
}
