import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { UserStoreService } from '../store/user-store.service';
import { UserService } from '../service/user.service';
import { User } from '../models/user';
import { hasAuthority } from '../helpers/authority-helpers';
import { ParamsService } from '../config/params.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthoritiesGuard implements CanActivate {
  constructor(private router: Router, private userStore: UserStoreService, private userService: UserService, private paramsService: ParamsService, private http: HttpClient, private HandleError: ConfigService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let roles = next.data["roles"] as Array<string>;
    if (roles == null) {
      return true;
    } else {
      //test for authorities
      let user: User = this.userStore.getCurrentUser();
      if (user != null) {
        //if user exist already in the store
        if (hasAuthority(user, roles)) {
          return true;
        } else {
          this.router.navigate(['/denied']);
          return false
        }
      } else {
        //get current user from db
        return this.http.get(this.paramsService.getUrl() + 'api/user/current')
          .map((data: User) => {
            if (hasAuthority(data, roles)) {
              return true;
            } else {
              this.router.navigate(['/denied']);
              return false
            }
          })
          .catch((error: HttpErrorResponse) => {
            this.HandleError.handleError(error);
            this.router.navigate(['/denied']);
            return Observable.of(false);
          })
          ;
      }
    }
  }
}
