import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = '';
        let contentType = 'application/json';

        //gettoken
        if (localStorage.getItem('token')) {
            token = localStorage.getItem('token');
        }
        let value: any = {
            headers: req.headers
                .set('Content-Type', contentType)
                .set('Authorization', token)
                .set('Accept', 'application/json')
                .set('Access-Control-Allow-Origin', '*')
        };
        let copiedReq = req.clone({ headers: value.headers });

        //for multipart data
        if (req.headers.get('Content-Type')=="multipart/form-data") {
            let newHeaders= value.headers;
            newHeaders=newHeaders.delete('Content-Type');
            copiedReq= req.clone({headers: null});
            copiedReq= copiedReq.clone({ headers: newHeaders });
        }
        return next.handle(copiedReq);
    }
}