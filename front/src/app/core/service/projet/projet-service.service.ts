import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Projet } from '../../models/projet';

@Injectable()
export class ProjetServiceService {

  constructor(private http:HttpClient) { }
  private _RefreshProjet = new Subject<void>();
  get RefreshProjet(){
  return this._RefreshProjet;
  }

  getAllProje(){
    return this.http.get('http://localhost:8001/projets')
  }
  getOneProje(id:number){
    // return this.http.get('http://localhost:8081/api/projets/'+id)
    return this.http.get(`http://localhost:8001/projets/${id}`)
  }

  addProje(projet:Projet){
    return this.http.post('http://localhost:8001/projets',projet)
    .pipe(
      tap(()=>{
      this._RefreshProjet.next();
      })
      ); 
  }
  updateProjes(projet:Projet){
    return this.http.put('http://localhost:8001/projet',projet)
  }
  deleteProjes(id:number){
    return this.http.delete(`http://localhost:8001/projets/${id}`,{responseType:'text'}
    )
    .pipe(
      tap(()=>{
      this._RefreshProjet.next();
      })
      ); 
  }

}
