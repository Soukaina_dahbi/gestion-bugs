import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Bug } from '../../models/bug';
const headr={
  headers:new HttpHeaders({
    'Content-Type':'application/json'
  })
}
@Injectable()
export class BugService {

  private baseUrl = 'http://localhost:8001/bug';

  constructor(private http:HttpClient) { }
  private _RefreshBug = new Subject<void>();
get RefreshBug(){
return this._RefreshBug ;
}

  getAllBugs():Observable<Bug[]>{
    return this.http.get<Bug[]>(this.baseUrl)
  }
  getAllBugsPro(id:number):Observable<Bug[]>{
    return this.http.get<Bug[]>(`http://localhost:8001/projets/${id}/bug`)
  }
  getOneBug(id:number):Observable<Bug>{
    return this.http.get<Bug>(`${this.baseUrl}/${id}`)
  }
  

  addBug(bug:Bug, idProjet :Number):Observable<any>{
    console.log(idProjet)
  
    return this.http.post(`${this.baseUrl}/${idProjet}`,bug,headr)
    .pipe(
      tap(()=>{
      this._RefreshBug.next();
      })
      ); 
  }
  updateBugs(bug:Bug){
    return this.http.put(this.baseUrl,bug)
  }
  deleteBugs( idB:number){
    return this.http.delete(`${this.baseUrl}/${idB}`,{responseType:'text'})
    .pipe(
      tap(()=>{
      this._RefreshBug.next();
      })
      ); 
  }
}
