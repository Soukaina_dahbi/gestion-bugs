import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyModalComponent } from '../components/my-modal/my-modal.component';

@NgModule({
  imports: [CommonModule],
  exports: [CommonModule ,MyModalComponent],
  declarations: [MyModalComponent]
})
export class ModalImportModule { }
