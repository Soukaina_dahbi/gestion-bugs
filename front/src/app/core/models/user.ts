import { AuthoritiesEnum } from "../enum/authorities-enum.enum";

export interface User {
    id?: number,
    email: string,
    password?: string,
    firstname: string,
    lastname: string,
    fullName?: string,
    image?: string,
    enabled: boolean,
    avatar?: string,
    authorities: Authorities[],
    lastPasswordResetDate?: Date
}
export interface Authorities{
    id?: number,
    name: AuthoritiesEnum
}