import { Projet } from './projet';

export interface Bug {
idB?:number,
interfacee:String,
type:String,
date:Date,
statut:String,
projet:Projet

}
