import { FormControl, FormGroup } from "@angular/forms";

export function NumberValidation(control: FormControl): { [s: string]: boolean } {
    if (control.value >= 0) {
        return null;
    }
    return { 'numberIsNotValid': true };
}


export function LongValidation(control: FormControl): { [s: string]: boolean } {
    var reg = new RegExp(/^\d+$/);
    if (control.value!=null) {
        if (reg.test(control.value)) {
            return null;
        }
    }
    return { 'numberIsNotValid': true };
}
