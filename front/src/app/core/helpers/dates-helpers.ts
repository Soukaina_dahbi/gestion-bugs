import { dateToString } from "../mapping/DateMapping";

export function fromAndToToArray(from: Date, to: Date) {
    let startMonth: number = from.getMonth() + 1;
    let endMonth: number = 12;
    let startYear: number = from.getFullYear();
    let endYear: number = to.getFullYear();
    let arr = [];
    for (let index = startYear; index <= endYear; index++) {
        if (startYear == endYear) {
            endMonth = to.getMonth() + 1;
        }
        for (let index2 = startMonth; index2 <= endMonth; index2++) {
            arr.push({ val: index + '-' + BigMonth(index2) + '-01', abv: dateToString(index2) + ' ' + index })
        }
        startMonth = 1;
    }
    return arr;
}
export function BigMonth(value: number): string {
    if (value < 10) {
        return '0' + value;
    }
    return value + '';
}

export function BigYear(value: number): string {
    if (value.toString().length < 2) {
        return '000' + value;
    } else if (value.toString().length < 3) {
        return '00' + value;
    } else if (value.toString().length < 4) {
        return '0' + value;
    } else {
        return value.toString()
    }
}