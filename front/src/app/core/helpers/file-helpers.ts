export function S2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}
export function BlobFromStringByte(res: string) {
    let blob = new Blob([S2ab(atob(res))], {
        type: ''
    });
    return blob;
}