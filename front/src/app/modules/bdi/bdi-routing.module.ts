import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialMenuModule } from '../../core/modules/material-import.module';
import { ModalImportModule } from '../../core/modules/modal-import.module';

import { GridProjetComponent } from './components/projet/grid-projet/grid-projet.component';
import { BugsComponent } from './components/Bug/bugs/bugs.component';




const routes: Routes = [
  { path: '', component: GridProjetComponent, pathMatch: 'full'},
  { path: 'GridProjet', component: GridProjetComponent , pathMatch: 'full'},
  { path: 'GridBug', component: BugsComponent, pathMatch: 'full'},
  { path: 'projets/:id/bugs', component: BugsComponent, pathMatch: 'full' }
  // { path: 'edit', component: EditLicenceComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalImportModule,
    MaterialMenuModule,
    RouterModule.forChild(routes)],

  exports: [
    MaterialMenuModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    
   
  ]
})
export class BdiRoutingModule { }
