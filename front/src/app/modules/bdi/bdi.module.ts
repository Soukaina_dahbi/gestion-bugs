import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BdiRoutingModule } from './bdi-routing.module';
import { MaterialMenuModule } from '../../core/modules/material-import.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GridProjetComponent } from './components/projet/grid-projet/grid-projet.component';
import { AddProjetComponent } from './components/projet/add-projet/add-projet.component';
import { EdiProjetComponent } from './components/projet/edi-projet/edi-projet.component';
import { BugsComponent } from './components/Bug/bugs/bugs.component';
import { DeleteProjetComponent } from './components/projet/delete-projet/delete-projet.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialMenuModule,
    BdiRoutingModule,
  ],
  declarations: [GridProjetComponent,BugsComponent
  ],
  providers: []
})
export class BdiModule { }
