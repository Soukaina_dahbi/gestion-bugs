import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Projet } from '../../../../../core/models/projet';
import { EdiProjetComponent } from '../edi-projet/edi-projet.component';
import { ProjetServiceService } from '../../../../../core/service/projet/projet-service.service';
import { AddProjetComponent } from '../add-projet/add-projet.component';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';
import { DeleteProjetComponent } from '../delete-projet/delete-projet.component';
@Component({
  selector: 'app-grid-projet',
  templateUrl: './grid-projet.component.html',
  styleUrls: ['./grid-projet.component.css']
})
export class GridProjetComponent implements OnInit {
projets:Projet[];
idProjet:number;
projet:Projet;
firstname:String;
pageActual:number=1;
searchterm:string;
  constructor(private toastr: ToastrService,private dialog: MatDialog,private projetService:ProjetServiceService) { }

  ngOnInit() {
    this.projetService.RefreshProjet
    .subscribe(() =>{
    this.getAllProjets();
    });
    this.getAllProjets();
  }
  Search(){
    if(this.firstname !=""){
      this.projets=this.projets.filter(res=>{
        return res.nom.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase())
      });
    }else if(this.firstname == ""){
      this.ngOnInit();
    }
   
  }
  getAllProjets(){
    this.projetService.getAllProje().subscribe((projets:Projet[])=>{
      this.projets= projets;
          })
  }
  openDialog(): void {
    let dialogRef = this.dialog.open(AddProjetComponent);
  }

  openDialogEdit(id:number){
    let dialogRef = this.dialog.open(EdiProjetComponent,{
      data: {id: id}
    });
    dialogRef.afterClosed().subscribe(()=>{
      this.getAllProjets();
    })
  }

  deleteProjet(projet){
    let dialogRef = this.dialog.open(DeleteProjetComponent,{
      data: {id: projet.id}
    });
    // this.projetService.deleteProjes(id).subscribe(()=>{
    //   this.toastr.success(SuccessMapping('!!! votre Projet Supprimer avec succès'));
    // this.getAllProjets();
    // })
  }
}
