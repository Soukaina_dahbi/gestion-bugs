import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Projet } from '../../../../../core/models/projet';
import { ProjetServiceService } from '../../../../../core/service/projet/projet-service.service';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';
@Component({
  selector: 'app-add-projet',
  templateUrl: './add-projet.component.html',
  styleUrls: ['./add-projet.component.css']
})
export class AddProjetComponent implements OnInit {
projet:Projet={
  nom:"",
  date: new Date()
};
  constructor(private projetService:ProjetServiceService,private router:Router,private dialog:MatDialog,
    public dialogRef: MatDialogRef<AddProjetComponent> ,private toastr: ToastrService) { }

  ngOnInit() {
  }

  onSaveProject(form){
    if(form.valid){
      this.projetService.addProje(this.projet).subscribe(()=>{
        
        this.toastr.success(SuccessMapping('votre Projet ajouter avec succée'));
        this.dialogRef.close();
        this.router.navigate(['/bdi/GridProjet'])
      })
    }
  }
}
