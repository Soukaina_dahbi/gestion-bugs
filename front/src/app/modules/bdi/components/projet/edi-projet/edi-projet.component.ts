import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Projet } from '../../../../../core/models/projet';
import { ProjetServiceService } from '../../../../../core/service/projet/projet-service.service';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';
@Component({
  selector: 'app-edi-projet',
  templateUrl: './edi-projet.component.html',
  styleUrls: ['./edi-projet.component.css']
})
export class EdiProjetComponent implements OnInit {
id:number;
projet:Projet={
  nom:"",
  date:new Date()
};
  constructor(private toastr: ToastrService,private projetService:ProjetServiceService ,private router:Router, public dialogRef: MatDialogRef<EdiProjetComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      this.id = data.id
    }
    
  ngOnInit() {
    this.projetService.getOneProje(this.id).subscribe((projet:Projet)=>{
      this.projet = projet;
    })
  }
  cancel(){
    this.dialogRef.close();
  }
  onEditProject(form){
    if(form.valid){
      this.projetService.updateProjes(this.projet).subscribe(()=>{
        this.toastr.success(SuccessMapping('votre Projet Modifié avec succès'));
        this.dialogRef.close();
        this.router.navigate(['/bdi/GridProjet'])
      })
    }
  }

}
