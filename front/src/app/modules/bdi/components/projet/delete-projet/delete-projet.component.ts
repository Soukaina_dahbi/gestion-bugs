import { Component, OnInit, Inject } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';
import { ProjetServiceService } from '../../../../../core/service/projet/projet-service.service';
import { Projet } from '../../../../../core/models/projet';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { inject } from '@angular/core/src/render3';

@Component({
  selector: 'app-delete-projet',
  templateUrl: './delete-projet.component.html',
  styleUrls: ['./delete-projet.component.css']
})
export class DeleteProjetComponent implements OnInit {
  projets:Projet[];
  constructor(private toastr: ToastrService,private projetService:ProjetServiceService,private router:Router, public dialogRef: MatDialogRef<DeleteProjetComponent>
   , @Inject(MAT_DIALOG_DATA) public data:any
    ) { }

  ngOnInit() {
  
  }
 
  deleteP(){
    this.projetService.deleteProjes(this.data.id).subscribe(()=>{
      this.toastr.success(SuccessMapping('!!! votre Projet Supprimer avec succès'));
     
      this.dialogRef.close();
    })
  }
  onAnnuler() {
    this.dialogRef.close();
  }

}
