import { Component, OnInit,Output ,EventEmitter} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {HttpClient} from '@angular/common/http';

import { MatDialog } from '@angular/material';

import { Bug } from '../../../../../core/models/bug';
import { BugService } from '../../../../../core/service/Bug/bug.service';
import { MyDialogBugComponent } from '../my-dialog-bug/my-dialog-bug.component';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-bugs',
  templateUrl: './bugs.component.html',
  styleUrls: ['./bugs.component.css']
})

export class BugsComponent implements OnInit {
  projetId;
  type:string;
  bugs
  @Output() addBugEvent: EventEmitter<any> = new EventEmitter()
  constructor(
    private toastr: ToastrService,
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private http:HttpClient,
    private bugService:BugService
    ) { }

  ngOnInit() {
    this.bugService.RefreshBug
    .subscribe(() =>{
    this.getBugs(this.projetId);
    });
    this.activatedRoute.params.subscribe(p => {
      this.projetId = p['id']
     this.getBugs(this.projetId)}
    )
   
  }
  openDialog(): void {
    let dialogRef = this.dialog.open(MyDialogBugComponent);
    
  }
  // onDeleteBug(bg){
  //   let dialogRef = this.dialog.open(DeleteBugComponent,{
  //     data: {id: bg.idB}
  //   });
   
  // }
  

  onEditug(bug: Bug) {
   
    let dialogRef = this.dialog.open(MyDialogBugComponent, {
      data: bug
    });
  } 
  getBugs(projetId)
  {
     this.http.get("http://localhost:8001/projets/" + this.projetId +"/bug")
       .subscribe(data=>{
            this.bugs=data;
        },err=>{
          // console.log(err);
        })
   }
   Search(){
    if(this.type !=""){
      this.bugs=this.bugs.filter(res=>{
        return res.type.toLocaleLowerCase().match(this.type.toLocaleLowerCase())
      });
    }else if(this.type == ""){
      this.ngOnInit();
    }

  }
}
