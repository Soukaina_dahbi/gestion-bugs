import { Component, OnInit, Output,EventEmitter, Inject } from '@angular/core';


import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Bug } from '../../../../../core/models/bug';

@Component({
  selector: 'app-my-dialog-bug',
  templateUrl: './my-dialog-bug.component.html',
  styleUrls: ['./my-dialog-bug.component.css']
})
export class MyDialogBugComponent implements OnInit {
  @Output() addBugEvent: EventEmitter<any> = new EventEmitter()
  bug: Bug;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Bug
  ) { }

  ngOnInit() {
    this.bug = this.data;
  }

  onAddBug($event){
    this.addBugEvent.emit($event);
  }
}
