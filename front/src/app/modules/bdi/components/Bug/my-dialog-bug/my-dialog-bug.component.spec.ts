import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDialogBugComponent } from './my-dialog-bug.component';

describe('MyDialogBugComponent', () => {
  let component: MyDialogBugComponent;
  let fixture: ComponentFixture<MyDialogBugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDialogBugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDialogBugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
