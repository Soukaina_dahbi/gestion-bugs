import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Bug } from '../../../../../core/models/bug';
import { Projet } from '../../../../../core/models/projet';
import { BugService } from '../../../../../core/service/Bug/bug.service';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';
@Component({
  selector: 'app-add-bug',
  templateUrl: './add-bug.component.html',
  styleUrls: ['./add-bug.component.css']
})
export class AddBugComponent implements OnInit {

  @Input('bug') selectedBug: Bug;
  @Output() addBugEvent: EventEmitter<any> = new EventEmitter()
  projetId;
  bug: Bug;
  projet :Projet;
  isUpdate: boolean = false;
  bugs:Bug[]=[];
  id_projet: Number;
  public bugFile :any =File;
  selectedFile=null;
  constructor(
    private toastr: ToastrService, 
    private router:Router ,
    private bugService: BugService,
    private activatedRoute: ActivatedRoute,
    private dialogRef:MatDialogRef<AddBugComponent>
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(p => this.projetId = p['id'])
    
    this.initBug();
    if(this.selectedBug) {
      this.bug = this.selectedBug;
      this.isUpdate = true;
    }
  }
  onSelectFile(event){
    this.selectedFile=event.target.files[0];
   
  }
  cancel() {
    this.dialogRef.close();
  }

  onSaveBug() {
    if(this.isUpdate) {
      this.bugService.updateBugs(this.bug)
        .subscribe(
          response => {
            this.toastr.success(SuccessMapping('votre bug Modifer avec succès'));
            this.initBug();
            this.cancel();
          }
        )
    } else {
   
      this.bugService.addBug(this.bug,this.id_projet )
  
        .subscribe(
          response => {
    
            console.log(response);
            this.addBugEvent.emit(response);
          
            this.toastr.success(SuccessMapping('votre bug Ajouter avec succès'));
            this.initBug();
            this.cancel();
                        
          }
        )
    }
  }

  initBug() {
    this.bug = {idB:null,interfacee:'', type:'', date:new Date(),statut:'',projet:this.projetId};
  }

}
