import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { NewUserComponent } from './components/user/new-user/new-user.component';
import { EditUserComponent } from './components/user/edit-user/edit-user.component';
import { RoleUserComponent } from './components/user/role-user/role-user.component';
import { ProfilComponent } from './components/profil/profil.component';
import { FichierUserComponent } from './components/profil/fichier-user/fichier-user.component';
import { AvatarUserComponent } from './components/profil/avatar-user/avatar-user.component';
import { PasswordUserComponent } from './components/profil/password-user/password-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthoritiesGuard } from '../../core/auth/authorities.guard';
import { ModalImportModule } from '../../core/modules/modal-import.module';
import { MaterialMenuModule } from '../../core/modules/material-import.module';

const routes: Routes = [
  { path: '', component: UserComponent, pathMatch: 'full', canActivate: [AuthoritiesGuard], data: { roles: ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN'] } },
  { path: 'new', component: NewUserComponent, pathMatch: 'full', canActivate: [AuthoritiesGuard], data: { roles: ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN'] } },
  { path: 'edit/:id', component: EditUserComponent, canActivate: [AuthoritiesGuard], data: { roles: ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN'] } },
  { path: 'profil', component: ProfilComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalImportModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MaterialMenuModule
  ],
  exports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialMenuModule
  ],
  declarations: [
    UserComponent,
    NewUserComponent,
    EditUserComponent,
    RoleUserComponent,
    ProfilComponent,
    FichierUserComponent,
    AvatarUserComponent,
    PasswordUserComponent]
})
export class UserRoutingModule { }
