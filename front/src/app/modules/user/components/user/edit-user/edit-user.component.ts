import { Component, OnInit } from '@angular/core';
import { UserStoreService } from '../../../../../core/store/user-store.service';
import { UserService } from '../../../../../core/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { User, Authorities } from '../../../../../core/models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthoritiesEnum } from '../../../../../core/enum/authorities-enum.enum';
import { ObjectKeys } from '../../../../../core/helpers/json-helpers';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  user: User = null;
  userId: string;
  isLoaded: boolean = false;
  userForm: FormGroup;
  authorities: any[];
  constructor(private route: ActivatedRoute, private router: Router, private userStore: UserStoreService, private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.route.params.subscribe((data) => {
      this.userId = data.id;
      this.filldata();
    });
    let authEnum: typeof AuthoritiesEnum = AuthoritiesEnum;
    let arr = [];
    ObjectKeys(AuthoritiesEnum).forEach(key => {
      arr.push({
        'name': authEnum[key],
        'value': {
          'name': key
        }
      })
    });
    this.authorities = arr;
    this.userForm = new FormGroup({
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'email': new FormControl('', [Validators.required, Validators.email, Validators.maxLength(150)]),
      'password': new FormControl(null, [Validators.minLength(6), Validators.maxLength(100)]),
      'enabled': new FormControl(true, [Validators.required]),
      'authorities': new FormControl(null),
    });
  }
  filldata() {
    this.isLoaded = false;
    this.userService.getOne(this.userId).subscribe(
      (data) => {
        this.user = data;
        this.patchValues();
        this.isLoaded = true;
      },
      (error: HttpErrorResponse) => {
        this.router.navigate(['/user']);
      }
    );
  }

  patchValues() {
    let auth = null;
    if (this.user.authorities != null) {
      auth = [];
      let arr: any[] = this.user.authorities;
      if (arr.length != 0) {
        this.user.authorities.map(data => {
          auth.push(data.name);
        });
      }
    }
    this.userForm.patchValue({
      'firstname': this.user.firstname,
      'lastname': this.user.lastname,
      'email': this.user.email,
      'enabled': this.user.enabled,
      'authorities': auth,
    });
  }
  onSubmit() {
    this.user.firstname = this.userForm.value.firstname;
    this.user.lastname = this.userForm.value.lastname;
    this.user.email = this.userForm.value.email;
    this.user.enabled = this.userForm.value.enabled;
    this.user.authorities = null;
    if (this.userForm.value.authorities != null) {
      if (this.userForm.value.authorities.length != 0) {
        this.user.authorities = [];
        this.userForm.value.authorities.map(data => {
          this.user.authorities.push({
            'name': data
          });
        });
      }
    }
    if (this.userForm.value.password != null && this.userForm.value.password != '') {
      this.user.password = this.userForm.value.password;
    }
    this.isLoaded = false;
    this.userService.editUser(this.user).subscribe(
      (data) => {
        this.toastr.success(SuccessMapping('USER_EDITED'));
        this.router.navigate(['/user']);
        this.isLoaded = true;
      },
      (error) => {
        this.isLoaded = true;
      }
    );
    return false;
  }
  onAnnuler() {
    this.router.navigate(['/user']);
    return false;
  }
}
