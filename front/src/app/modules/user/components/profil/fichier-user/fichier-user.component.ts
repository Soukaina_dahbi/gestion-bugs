import { Component, OnInit } from '@angular/core';
import { User } from '../../../../../core/models/user';
import { UserStoreService } from '../../../../../core/store/user-store.service';
import { UserService } from '../../../../../core/service/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-fichier-user',
  templateUrl: './fichier-user.component.html',
  styleUrls: ['./fichier-user.component.css']
})
export class FichierUserComponent implements OnInit {
  user: User = null;
  isLoaded: boolean = false;
  userForm: FormGroup;
  constructor(private userStore: UserStoreService, private userService: UserService,  private toastr: ToastrService) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'email': new FormControl('', [Validators.required, Validators.email, Validators.maxLength(150)]),
    });
    this.userStore.userMessage.subscribe(
      (user: User) => {
        if (user) {
          this.user = user;
          this.isLoaded = true;
          this.fillForm(user);
        } else {
          this.isLoaded = false;
          this.user = null;
        }
      }
    );
  }

  onSubmit() {
    this.isLoaded = false;
    let curUser={
      'id': this.user.id,
      'email': this.userForm.value.email,
      'password': this.user.password,
      'firstname': this.userForm.value.firstname,
      'lastname': this.userForm.value.lastname,
      'image': this.user.image,
      'enabled': this.user.enabled,
      'avatar': null,
      'authorities': this.user.authorities,
      'lastPasswordResetDate': this.user.lastPasswordResetDate
  }
    this.userService.changeInfo(curUser).subscribe(
      (user: User) => {
        this.user = user;
        this.isLoaded = true;
        this.fillForm(user);
        this.userStore.updateUser(user);
        this.toastr.success(SuccessMapping('USER_DATA_CHANGED'));
      },
      (error) => {
        this.user= this.userStore.getCurrentUser();
        this.isLoaded = true;
      }
    );
    return false;
  }

  onAnnuler() {
    if (this.user) {
      this.fillForm(this.user);
    }
    return false;
  }

  fillForm(user: User) {
    this.userForm.patchValue({
      'email': user.email,
      'firstname': user.firstname,
      'lastname': user.lastname
    });
  }

}
