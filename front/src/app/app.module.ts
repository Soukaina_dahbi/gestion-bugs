import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule, AppRoutingComponent, AppRoutingproviders } from './app-routing.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { AppComponent } from './app.component';
import { HomeComponent } from './core/components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FileSaverModule } from 'ngx-filesaver';
import { ToastrModule } from 'ngx-toastr';
import {MatListModule} from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatNativeDateModule, MatSnackBarModule } from '@angular/material';
import {MatInputModule} from '@angular/material'
import {MatExpansionModule} from '@angular/material/expansion';
import { ProjetServiceService } from './core/service/projet/projet-service.service';
import { EdiProjetComponent } from './modules/bdi/components/projet/edi-projet/edi-projet.component';
import { AddProjetComponent } from './modules/bdi/components/projet/add-projet/add-projet.component';
import { BugService } from './core/service/Bug/bug.service';
import { AddBugComponent } from './modules/bdi/components/Bug/add-bug/add-bug.component';
import { MyDialogBugComponent } from './modules/bdi/components/Bug/my-dialog-bug/my-dialog-bug.component';
import { DeleteProjetComponent } from './modules/bdi/components/projet/delete-projet/delete-projet.component';



@NgModule({
  declarations: [
    AppComponent,
    AppRoutingComponent,
    HomeComponent,
  AddProjetComponent,
  EdiProjetComponent,
  AddBugComponent, 
  MyDialogBugComponent,
  DeleteProjetComponent,
 
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ToastrModule.forRoot({timeOut: 3000}),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FileSaverModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatInputModule,
    MatExpansionModule,
   
  ],
  providers: [
  AppRoutingproviders,ProjetServiceService,BugService],
  bootstrap: [
    AppComponent
  ],

  entryComponents: [
  AddProjetComponent,EdiProjetComponent,AddBugComponent,MyDialogBugComponent,DeleteProjetComponent
  ],
})
export class AppModule { }
